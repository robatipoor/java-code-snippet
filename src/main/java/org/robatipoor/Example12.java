
package org.robatipoor;

/**
 * ShallowCopy
 */
public class Example12 {

    public static void main(String[] args) throws CloneNotSupportedException {
        var courseJava = new Course("Java");
        var student = new Student(1, "studentName", courseJava);
        var shallowCopyStudent = (Student) student.clone();
        shallowCopyStudent.course.subject = "Rust";
        System.out.println(student.course.subject);
    }
}

class Student implements Cloneable {
    int id;
    String name;
    Course course;

    public Student(int id, String name, Course course) {
        this.id = id;
        this.name = name;
        this.course = course;
    }

    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

class Course implements Cloneable {
    String subject;

    public Course(String subject) {
        this.subject = subject;
    }

    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
