
package org.robatipoor;

import java.util.concurrent.CountDownLatch;
import java.util.stream.IntStream;

/**
 * DataRace2
 */
public class Example10 {

    private static int counter;

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch cdl = new CountDownLatch(3);
        var th1 = new Thread(() -> {
            IntStream.range(0, 1000).forEach((x) -> {
                counter++;
            });
            cdl.countDown();
        });
        var th2 = new Thread(() -> {
            IntStream.range(0, 1000).forEach((x) -> {
                counter++;
            });
            cdl.countDown();
        });
        var th3 = new Thread(() -> {
            IntStream.range(0, 1000).forEach((x) -> {
                counter++;
            });
            cdl.countDown();
        });
        th1.start();
        th2.start();
        th3.start();
        cdl.await();
        System.out.println(counter);

    }
}
