
package org.robatipoor;

import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Convertable
 */
public class Example17 {

    public static void main(String[] args) {
        Convertable c = (s1, s2) -> {
            if (s1.length() != s2.length()) {
                return false;
            }
            Set<Character> chs1 = s1.chars().mapToObj(x -> (char) x).collect(Collectors.toSet());
            Set<Character> chs2 = s1.chars().mapToObj(x -> (char) x).collect(Collectors.toSet());
            var hms1 = new HashMap<Character, Long>();
            var hms2 = new HashMap<Character, Long>();
            for (Character cc : chs1) {
                hms1.put(cc, s1.chars().filter((x) -> x == cc).count());
            }
            for (Character cc : chs2) {
                hms2.put(cc, s2.chars().filter((x) -> x == cc).count());
            }
            return hms1.equals(hms2);
        };
        System.out.println(c.isConvertable("AAAB", "BAAB"));
        System.out.println(c.isConvertable("AABE", "BAEB"));
        System.out.println(c.isConvertable("AAE", "BEB"));
        System.out.println(c.isConvertable("BBBE", "BEBB"));
    }
}

interface Convertable {
    boolean isConvertable(String s1, String s2);
}