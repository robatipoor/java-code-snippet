
package org.robatipoor;

/**
 * DataRace
 */
public class Example9 {

    private static boolean flag = false;

    public static void main(String[] args) {
        var th = new Thread(() -> {
            flag = true;
        });
        th.start();
        while(!flag); 
        System.out.println(flag);
    }
}

