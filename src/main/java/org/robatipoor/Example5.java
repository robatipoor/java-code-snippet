
package org.robatipoor;

import java.util.stream.Stream;

/**
 * Fibonacci
 */
public class Example5 {

    public static void main(String[] args) {
        Stream.iterate(new int[] { 0, 1 }, x -> new int[] { x[1], x[0] + x[1] }).limit(10).map(x -> x[0])
                .forEach(System.out::println);
    }
}