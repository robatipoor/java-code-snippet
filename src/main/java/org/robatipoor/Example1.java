
package org.robatipoor;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.stream.Collectors;

/**
 * EnumDataType
 */

public class Example1 {

    public static void main(String[] args) {
        for (Level level : Level.values()) {
            System.out.println(level);
        }
        EnumSet<Level> set = EnumSet.of(Level.One, Level.Two, Level.Tree);
        System.out.println(set);
        EnumMap<Level, String> map = new EnumMap<>(Level.class);
        map.put(Level.One, "One Number");
        map.put(Level.Two, "Two Number");
        map.put(Level.Tree, "Tree Number");
        System.out.println(map);
        var set2 =  Arrays.stream(Level.values()).collect(Collectors.toSet());
        System.out.println(set2);
    }
}

enum Level {
    One(1), Two(2), Tree(3);
    private Integer level;

    private Level(Integer level) {
        this.level = level;
    }

    public String toString() {
        return String.valueOf(this.level);
    }
}

