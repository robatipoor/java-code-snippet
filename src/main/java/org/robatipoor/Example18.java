
package org.robatipoor;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

/**
 * SchedulingTasks
 */
public class Example18 {

    public static void main(String[] args) throws InterruptedException {
        Timer timer = new Timer();
        CountDownLatch count = new CountDownLatch(1);
        Task task = new Task(count);
        timer.schedule(task, 1000);
        count.await();
        task.cancel();
        timer.cancel();
        System.out.println("End ...");
    }
}

class Task extends TimerTask {

    CountDownLatch count;

    public Task(CountDownLatch count) {
        this.count = count;
    }

    @Override
    public void run() {
        System.out.println("do task...");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        count.countDown();
    }
}
