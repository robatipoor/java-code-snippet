
package org.robatipoor;

/**
 * StaticMethod
 */
public class Example3 {

    public static void main(String[] args) {
        // Example3 ex3 = null;
        // ex3.method(); Compiler replace ex3 with Example3 class
        Example3.method();
    }

    public static void method() {
        System.out.println("Static Method");
    }
}
