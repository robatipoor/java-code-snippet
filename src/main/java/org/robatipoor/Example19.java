
package org.robatipoor;

/**
 * Inheritance
 */
public class Example19 {

    public static void main(String[] args) {
        Child child = new Child(); // Constructors are not members, so they are not inherited by subclasses
        System.out.println(child.name);
        Parent parent = (Parent) child;
        System.out.println(parent.name);
        Parent.staticMethod();
        Child.staticMethod();
    }
}

class Parent {
    String name = "Parent";

    public Parent() {

    }

    public Parent(String name) {
        this.name = name;
    }

    public static void staticMethod(String s) {
        System.out.println("Static method parent with parameter " + s);
    }

    public static void staticMethod() {
        System.out.println("Static method parent");
    }
}

class Child extends Parent {
    String name = "Child";

    public static void staticMethod() {
        System.out.println("static method child");
    }
}
