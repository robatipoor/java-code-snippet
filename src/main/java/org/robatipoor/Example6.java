
package org.robatipoor;

import java.util.stream.IntStream;

/**
 * IntStream
 */
public class Example6 {

    public static void main(String[] args) {
        IntStream.range(1, 10).forEach(System.out::println);
    }
}
