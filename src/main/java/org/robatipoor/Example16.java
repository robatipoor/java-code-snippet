
package org.robatipoor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * CancelTask
 */
public class Example16 {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Task task = new Task();
        executorService.submit(task);
        executorService.shutdown();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        task.cancel();
    }
}

class Task implements Runnable {
    private static volatile boolean flag;

    @Override
    public void run() {
        var currentThread = Thread.currentThread();
        while (!flag) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("run " + currentThread.getName() + " " + currentThread.getId());
        }
        System.out.println("thread " + currentThread.getName() + " " + currentThread.getId() + "finish");
    }

    public void cancel() {
        flag = true;
    }
}
