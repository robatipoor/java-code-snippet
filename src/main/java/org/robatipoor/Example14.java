
package org.robatipoor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Executors
 */
public class Example14 {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        // ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            executorService.submit(() -> {
                var name = Thread.currentThread().getName();
                var id = Thread.currentThread().getId();
                System.out.println("Step 1 name = " + name + " id = " + id);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Finish Step 1");
        for (int i = 0; i < 5; i++) {
            executorService.submit(() -> {
                var name = Thread.currentThread().getName();
                var id = Thread.currentThread().getId();
                System.out.println("Step 2 name = " + name + " id = " + id);
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        executorService.shutdown();
    }
}
