
package org.robatipoor;

import java.util.ArrayList;
import java.util.List;

/**
 * AutoboxingAndUnboxing
 */
public class Example4 {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(10); // AutoBoxing
        var i = Integer.valueOf(1);
        printNumber(i); // Unboxing
    }

    public static void printNumber(int i) {
        System.out.println(i);
    }
}
