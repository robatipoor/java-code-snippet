
package org.robatipoor;

/**
 * RaceCondition
 */
public class Example8 {

    private static volatile boolean flag = false;

    public static void main(String[] args) {
        var th = new Thread(() -> {
            flag = true;
        });
        th.start();
        while(!flag);// if comment this code race condition to happen 
        System.out.println(flag);
    }
}

